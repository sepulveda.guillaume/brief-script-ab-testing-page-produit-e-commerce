/* Ensemble des scripts JS utilisés pour passer de la version B à A */



/* Ajouter un bouton switch */

// Selectionner barre de nav 
let selectBar = document.querySelector('.collapse > ul');

// Ajouter le bouton switch
selectBar.insertAdjacentHTML('beforeend', '<li class="dropdown"><a class="dropdown-item dropdown-toggle text-danger switch" href="#">Switch</button>')



/* Evenement switch au click */

// Selectionner le boutton switch
selectButtonSwitch = document.querySelector('.switch');

// Evénement switch au click
let switchEvent = selectButtonSwitch.addEventListener("click", function() {
    allFunctions();
});

// Lance les fonctions au switch
function allFunctions() {
    let selectLogoPage = document.querySelector('.header-logo img');
    let attributeLogo = selectLogoPage.getAttribute('src');
    console.log(attributeLogo);
    if(attributeLogo === "img/logo.png") {
        removeSideBar();
        updateLogo();
        addImgTop();
        addImgBottoms();
        backToTopCat();
        updateButtons();
        remoteBorderRadius(); 
    } 
    else {
        document.location.reload();
    }
}



/* Modification du logo */

function updateLogo() {

    // Selectionner logo
    let selectLogo = document.querySelector('.header-logo img');

    // Modification de l'attribut src du logo
    const newLogo = "img/logo-default.png"
    selectLogo.setAttribute("src", newLogo);
}



/* Ajouter images carroussel */

function addImgTop () {

    // Selectionner les images
    let selectImg = document.querySelectorAll('.owl-carousel img');

    // Modification de l'attribut src des images
    const newImg = "img/imgcar.png";

    for (i = 0; i < selectImg.length; i++) {
    selectImg[i].setAttribute("src", newImg);
    }
}



/* Ajouer images Related Products */

function addImgBottoms () {

    //Selectionner les images
    let selectImgProducts = document.querySelectorAll('.masonry-loader > .products img');

    //  Ajouer les images via l'attribut src de l'image
    const newImgProduct1 = "img/img1.png";
    const newImgProduct2 = "img/img2.png";
    const newImgProduct3 = "img/img3.png";
    const newImgProduct4 = "img/img4.png";

    selectImgProducts[0].setAttribute("src", newImgProduct1);
    selectImgProducts[1].setAttribute("src", newImgProduct2);
    selectImgProducts[2].setAttribute("src", newImgProduct3);
    selectImgProducts[3].setAttribute("src", newImgProduct4);
}



/* Faire disparaître le menu de navigation */

function removeSideBar () {

    // Selectionner la sidebar
    let selectSideBarNav = document.querySelector('.container > .row > .col-lg-3');
    // Supprimer la sidebar 
    selectSideBarNav.remove(); 

    // Modifier répartition colonne card boostrap (col-lg-9 => col-lg-12) 
    let selectContentCard = document.querySelector('.container > .row > .col-lg-9');
    selectContentCard.classList.replace('col-lg-9', 'col-lg-12');
}



/*  Remonter la div des catégories sous la description de la card */

function backToTopCat() {

    // Selectionner les div descriptions de la card, categories et form
    let selectDesc = document.querySelector('.summary');
    let selectCat = document.querySelector('.product-meta');
    let selectForm = document.querySelector('.cart');

    // Insérer les catégories avant le formulaire
    selectDesc.insertBefore(selectCat, selectForm);

    // Selectionner paragraphe description card 
    let selectPCard = document.querySelector('.summary > .mb-5');
    selectPCard.classList.replace('mb-5', 'mb-4');
}



/* Centrer et modifier les boutons de formulaire */

function updateButtons () {

    // Selectionner les buttons
    let selectButtons = document.querySelector('.cart');

    // Appliquer le display flex et la position relative désirée à cette classe 
    selectButtons.classList.value = "cart d-flex flex-column align-items-center position-relative";
    selectButtons.style.top = "130px";

    // Modifier margin div boutton -/+ après l'avoir selectionné
    let selectButtonPlus = document.querySelector('.quantity');
    selectButtonPlus.style.margin = "0 0 25px 0"

    // Modifier button submit après l'avoir selectionné avec les paramètres voulus 
    let selectSubmit = document.querySelector('.cart button');
    selectSubmit.style.fontSize = "1rem";
    selectSubmit.style.padding = "1rem 1.6rem";
}



/* Enlever effet border radius subscribe */

function remoteBorderRadius() {

    // Récupérer la classe où sont appliquées les styles 
    let styleButton = document.querySelector('.input-group-append button');

    // Supprimer les effets border raidus sur la classe
    styleButton.style.borderRadius="0px";
}